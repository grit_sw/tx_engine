import engine.backoff_requests as requests
from logger import logger
from multiprocessing.dummy import Pool as ThreadPool
from queue import Queue
import arrow
from engine.messages import prosumer_switch_message
import json


def prosumer_query(prosumer_url, probe_id, name):
	prosumer_name = "{}-{}".format(probe_id, name)
	prosumer_url = prosumer_url.format(prosumer_name=prosumer_name)
	try:
		resp = requests.get(prosumer_url)
	except Exception as e:
		logger.exception(e)
		logger.warn('Error {} when retrieving prosumer \
			{}-{}'.format(e.args[0], probe_id, name))
		return None

	if resp.status_code == 200:
		response = resp.json()
		logger.info('Gotten Prosumer Details for {}-{}'.format(probe_id, name))
		return response

	logger.warn('Error {} when retrieving prosumer {}-{}'.format(resp.status_code, probe_id, name))
	return None


def energy_plan_query(energy_plan_url, plan_id, prosumer_id):
	energy_plan_url = energy_plan_url.format(plan_id=plan_id)
	try:
		resp = requests.get(energy_plan_url)
		print('\n\n\n\nEnergy plan response', resp.json())
	except Exception as e:
		logger.exception(e)
		logger.warn('Error {} when retrieving prosumer \
			{}'.format(e.args[0], prosumer_id))
		return None

	if resp.status_code == 200:
		response = resp.json()
		print(response)
		logger.info('Got energy plan info for {}'.format(prosumer_id))
		return response
	logger.warn('Error {} when retrieving energy plan info {}'.format(resp.status_code, prosumer_id))
	return None


def _energy_cost(prosumer_id, energy_rate, energy_consumed):
	try:
		energy_cost = float(energy_rate) * float(energy_consumed)
		logger.info('{} debited {}'.format(prosumer_id, energy_cost))
		return energy_cost
	except Exception as e:
		logger.exception(e)
		logger.info('Error while calculating energy cost for {}'.format(prosumer_id))
		return None


def account_debit(account_url, group_id, energy_cost, prosumer_id, energy_consumed):
	debit_params = {
		'group_id': group_id,
		'debitor_id': 'tx_engine',
		'amount': energy_cost,
		'energy_units': energy_consumed,
	}
	try:
		resp = requests.post(account_url, data=debit_params)
		print('\n\n\nAccount response = ', resp.json())
	except Exception as e:
		logger.exception(e)
		# handle this properly
		return True

	if resp.status_code == 200:
		logger.info('Prosumer {} Debited {}'.format(
			prosumer_id,
			energy_cost,
		))
		response = resp.json()
		print(response)
		return False
	elif resp.status_code == 403:
		# disconnect user insufficient funds
		logger.info('Prosumer {} Owes {}.'.format(
			prosumer_id, resp.json()['data']['pending_amount'])
		)
		return True
	else:
		logger.warn('Error {} when debiting prosumer {}'.format(resp.status_code, prosumer_id))
		return True

# TODO: send a disconnection message to keypad, meter and database if needed


class Billing(object):
	"""docstring for Billing"""

	def __init__(self, producer):
		super(Billing, self).__init__()
		self.prosumer_url = 'http://localhost:5900/prosumers/tx-engine/{prosumer_name}/'
		self.energy_plan_url = 'http://localhost:5900/energy_plans/tx-engine/{plan_id}/'
		self.account_url = 'http://localhost:5900/account/debit/'
		self.prosumer_queue = Queue()
		self.prosumer_response_queue = Queue()
		self.energy_plan_response_queue = Queue()
		self.energy_cost_queue = Queue()
		self.account_response_queue = Queue()
		self.prosumer_disconnection_topic = 'prosumer-disconnect'
		self.disconnect_producer = producer

	def delivery_report(self, err, msg):
		""" Called once for each message produced to indicate delivery result.
			Triggered by poll() or flush(). """
		if err is not None:
			logger.critical('Message delivery failed: {}'.format(err))
			# TODO resend
		else:
			logger.info('Message delivered to {} [{}]'.format(msg.topic(), msg.partition()))

	def disconnect_prosumer(self, probe_id, name):
		if not (probe_id and name):
			raise ValueError("probe_id and name required. probe_id = {}, name = {}".format(probe_id, name))
		logger.info('Sending Disconnection message for {}-{}\
		 at {}'.format(
			probe_id,
			name,
			arrow.now('Africa/Lagos'),
		))
		prosumer_switch_message['probe_id'] = probe_id
		prosumer_switch_message['sourceName'] = name
		prosumer_switch_message['switch'] = 'True'
		message = json.dumps(prosumer_switch_message)
		self.disconnect_producer.send_message(
			topic=self.prosumer_disconnection_topic,
			message=message,
			callback=self.delivery_report,
		)
		logger.info('Sent Disconnection message for {}-{}\
		 at {}'.format(
			probe_id,
			name,
			arrow.now('Africa/Lagos'),
		))

	def billing_job(self, queue_data):
		start_time = arrow.now()
		probe_id, name, energy_consumed, measured_power = queue_data
		logger.info('Starting Billing Job for {}-{} at {}'.format(probe_id, name, start_time))
		logger.info('Obtaining {}-{} prosumer data'.format(probe_id, name))
		from time import time
		t1 = time()
		prosumer_id = '{}-{}'.format(probe_id, name)
		prosumer_data = prosumer_query(self.prosumer_url, probe_id, name)
		print("\n\n\nProsumer for {} took {}".format(prosumer_id, time() - t1))
		if prosumer_data is None:
			logger.info('Disconnecting Prosumer {}: Error in retrieving prosumer data.'.format(prosumer_id))
			self.disconnect_prosumer(probe_id, name)
			return
		plan_id = prosumer_data['data']['plan_id']
		facility_id = prosumer_data['data']['facility_id']
		t2 = time()
		energy_plan = energy_plan_query(
			self.energy_plan_url,
			plan_id,
			prosumer_id
		)
		print("\n\n\nEnergyPlan for {} took {}".format(prosumer_id, time() - t2))
		if energy_plan is None:
			logger.info('Disconnecting Prosumer {}: Error in retrieving energy plan.'.format(prosumer_id))
			self.disconnect_prosumer(probe_id, name)
			return
		power_limit_is_set = energy_plan['data']['power_limit_is_set']
		power_limit = energy_plan['data']['power_limit']
		if power_limit_is_set:
			if measured_power > power_limit:
				logger.info('Disconnecting Prosumer {}: Power limit exceeded. {} vs {}'.format(prosumer_id, measured_power, power_limit))
				self.disconnect_prosumer(probe_id, name)
				return

		energy_rate = energy_plan['data']['rate']
		energy_cost = _energy_cost(
			prosumer_id,
			energy_rate,
			energy_consumed
		)
		if energy_cost is None:
			logger.info('Disconnecting Prosumer {}: Error in calculating Energy Cost.'.format(prosumer_id))
			self.disconnect_prosumer(probe_id, name)
			return
		group_id = prosumer_data['data']['user_group_id']
		if group_id is None:
			logger.info("Disconnecting Prosumer {}: Prosumer not assigned to a User".format(prosumer_id))
			self.disconnect_prosumer(probe_id, name)
			return
		t3 = time()
		to_disconnect = account_debit(
			self.account_url,
			group_id,
			energy_cost,
			prosumer_id,
			float(energy_consumed),
		)
		print("\n\n\nAccountDebit for {} took {}".format(prosumer_id, time() - t3))

		if to_disconnect:
			logger.info('Disconnecting Prosumer {}: Account response'.format(prosumer_id))
			self.disconnect_prosumer(probe_id, name)
		logger.info("\n")
		logger.info("\n")
		logger.info('Finished Billing Job for {} at {}. \
			Took {} seconds'.format(
			prosumer_id,
			arrow.now(),
			(arrow.now() - start_time).total_seconds())
		)
		return

	def bill_prosumers(self, job_queue):
		print('len(job_queue) ', len(job_queue))
		# thread_pool = ThreadPool(int(len(job_queue) / 2))
		thread_pool = ThreadPool(8)
		thread_pool.map(self.billing_job, job_queue)
		thread_pool.close()
