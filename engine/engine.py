from logger import logger
from time import time, sleep
from confluent_kafka import KafkaError
from confluent_kafka.avro import AvroConsumer
from confluent_kafka.avro.serializer import SerializerError
from engine.meter import ProsumerStream
from engine.billing import Billing
from collections import deque
from engine.producer import ProbeProducer


def getpower(source_name, power_prosumers):
    power_since_last = None
    for power_prosumer in iter(power_prosumers):
        if source_name == power_prosumer['sourceName']:
            power_since_last = power_prosumer['powerSinceLast']
            break
    return power_since_last


class TransactionEngine(object):
    """
        The TransactionEngine class that performs meter connection and billing management.
    """

    def __init__(self, bootstrap_servers, schema_registry, group_id, topics):
        """
            Init transaction engine parameters

                    The values can be found in config.py

            @:param: bootstrap_servers: Kafka Server(s) to connect to
            @:param: schema_registry: Schema registry url
            @:param: group_id: The consumer's group ID
            @:param: topics: A list of topics to consume from
        """
        super(TransactionEngine, self).__init__()
        self.bootstrap_servers = bootstrap_servers
        self.schema_registry = schema_registry
        self.group_id = group_id
        self.topics = topics
        self.running = True
        self.consumer = self.create_consumer()
        self.producer = ProbeProducer(
            bootstrap_servers='localhost',
        )

        self.probes = ProsumerStream(
            schema_registry=self.schema_registry,
            topic='probe-stream',
            schema_subject='meter_data_stream_v0',
            schema_file='stream-v0.avsc'
        )

    def create_consumer(self):
        """
            Method that creates a new avro consumer
            @:param: None
            @:returns: None
        """
        _consumer = AvroConsumer({
            'bootstrap.servers': self.bootstrap_servers,
            'group.id': self.group_id,
            'schema.registry.url': self.schema_registry,
            'default.topic.config': {'auto.offset.reset': 'smallest'},
        })
        _consumer.subscribe(self.topics)
        return _consumer

    def start_consumer(self):
        """
            Method that consumes messages from kafka and performs transaction logic
            @:param: None
            @:returns: None
        """
        logger.info('self.topics = {}'.format(self.topics))
        power_count = 1
        t1 = time()
        while self.running:
            try:
                msg = self.consumer.poll(0.01)
                if msg is None:
                    continue
                if not msg.error():
                    if msg.topic() == self.probes.topic:
                        message = msg.value()
                        if not isinstance(message, dict):
                            continue
                            # raise TypeError("Unrecognized message format")
                        try:
                            probe_id = message['master']['id']
                        except KeyError as key_error:
                            logger.warn('Bad Message {}'.format(message))
                            logger.exception(key_error)
                            continue
                        logger.info('Processing message from {}'.format(probe_id))
                        prosumers = message['data'][0]['energySinceLast']
                        power_prosumers = message['data'][0]['powerSinceLast']
                        # print(power_prosumers)
                        # list of prosumer names and energy consumed
                        # print(message)
                        # ----------------------------
                        # data = [
                        #     (prosumer['sourceName'], prosumer['energySinceLast'])
                        #     for prosumer in prosumers
                        # ]

                        prosumer_queue = deque()
                        for prosumer in prosumers:
                            source_name = prosumer['sourceName']
                            power_since_last = getpower(source_name, power_prosumers)
                            data = (
                                probe_id,
                                source_name,
                                prosumer['energySinceLast'],
                                power_since_last,
                            )
                            print(data)
                            prosumer_queue.append(data)
                        sbp = time()
                        Billing(self.producer).bill_prosumers(prosumer_queue)
                        print("\n\n\n\n\n")
                        logger.info("Billing Process Took {}".format(time() - sbp))
                        # -------------------------------

                        # for prosumer in prosumers:
                        #     print(prosumer['energySinceLast'], prosumer['sourceName'])
                        # energy_consumed = self.energy_consumed(message)
                        # energy_threshold = self.energy_threshold(
                        #     probe_id)
                        # power_usage = self.power_usage(message)
                        # power_threshold = self.power_threshold(probe_id)

                        # -----------------------------------------------------
                        # is_first_msg = self.first_msg_today(message)
                        # if is_first_msg:
                        #     if self.sufficient_funds(probe_id):
                        #         logger.info(
                        #             '\n\n\nFirst Message: debiting account')
                        #         self.debit_account(probe_id)
                        #     else:
                        #         if self.to_disconnect(probe_id):
                        #             info = 'Insufficient funds'
                        #             self.alert_user(probe_id, info)
                        #             self.send_meter_disconnect(
                        #                 probe_id, info)
                        # -----------------------------------------------------

                        # if energy_consumed > energy_threshold:
                        #     if self.to_disconnect(probe_id):
                        #         info = 'Energy usage exceeded'
                        #         # logger.info("\n{}".format(info))
                        #         self.alert_user(probe_id, info)
                        #         self.send_meter_disconnect(probe_id, info)

                        # -----------------------------------------------------
                        # if power_usage > power_threshold:
                        #     if self.to_disconnect(probe_id):
                        #         info = 'Power limit exceeded'
                        #         logger.info("\n{}".format(info))
                        #         self.alert_user(probe_id, info)
                        #         self.send_meter_disconnect(probe_id, info)
                        # -----------------------------------------------------
                        """
                            self.set_meter_state(probe_id, state)
                            Helps to flag meters that are ON.
                            If a message is received from a customer,
                            it means the customer is connected regardless
                            of the switching state the customer is meant to be in.
                            Likewise, the customer is in an ON state after the
                            disconnect method is called till a response is obtained.
                            Useful for methods that check the meter states.
                            Think hard before changing this
                        """
                        # self.set_meter_state(probe_id, state=True)
                        # self.record_message_time(message)

                        total_time = time() - t1
                        logger.info("\n")
                        logger.info("\n")
                        logger.info("Consumed {} power data messages".format(power_count))
                        logger.info("Consumption Velocity = {} messages per second".format(power_count / total_time))
                        logger.info("Consumption Duration = {} seconds".format(total_time))
                        logger.info("Consumption Duration = {} minutes".format(total_time / 60))
                        logger.info("\n")
                        logger.info("\n")
                        power_count += 1
                        sleep(0)

                    # break
                    # logger.info(msg.value())
                elif msg.error().code() != KafkaError._PARTITION_EOF:
                    logger.exception(msg.error())
                    continue
                    # self.running = False
            except SerializerError as e:
                logger.exception(
                    "Message deserialization failed for {}: {}".format(msg, e))
                # skip serialization errors
                continue
            # finally:
            #     total_time = time() - t1
            #     logger.info("\n\tConsumed {} messages".format(message_count))
            #     logger.info("Consumption Velocity = {} messages per second".format(message_count / total_time))
            #     logger.info("Consumption Duration = {} seconds".format(total_time))
            #     logger.info("Consumption Duration = {} minutes\n".format(total_time / 60))
            #     message_count += 1
            #     sleep(0)

        self.consumer.close()

    def run(self):
        """
            Method that starts the transaction engine
            @:param: None
            @:returns: None
        """
        logger.info('Starting Transaction Engine')
        self.start_consumer()
