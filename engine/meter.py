from logger import logger
from confluent_kafka.avro import CachedSchemaRegistryClient as SchemaRegistry
from engine.register_schema import RegisterSchema


class ProsumerStream(object):
    """docstring for ProsumerStream"""

    def __init__(self, schema_registry, topic, schema_subject, schema_file):
        """
            @:param: schema_registry: Schema registry url
            @:param: meter_data_topic: The topic where electricity data are streamed through
            @:param: schema_subject: meter data stream subject
            @:param: schema_file: The meter data avro schema file
        """
        super(ProsumerStream, self).__init__()
        self.schema_registry = schema_registry
        self.topic = topic
        self.schema_subject = schema_subject
        self.schema_file = schema_file
        self.configure_stream_schema()

    def configure_stream_schema(self):
        """
            Method that registers a new avro data stream schema if none exists
            @:param: None
            @:returns: None
        """
        _, stream_schema, _ = SchemaRegistry(
            self.schema_registry).get_latest_schema(self.schema_subject)
        if stream_schema is None:
            logger.info('Registering meter_stream schema')
            register_schema = RegisterSchema(
                self.schema_registry,
                self.schema_subject,
                self.schema_file
            )
            register_schema.post_schema()
        logger.info('stream_schema schema registered at {}'.format(self.schema_registry))
