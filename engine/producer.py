from time import sleep, time
from confluent_kafka import Producer
from confluent_kafka.avro import AvroProducer
from confluent_kafka.avro import CachedSchemaRegistryClient as SchemaRegistry


class ProducerAvro(object):
    """docstring for ProducerAvro"""

    def __init__(self, bootstrap_servers=None, schema_registry=None, group_id=None, topic=None, schema_subject=None):
        super(ProducerAvro, self).__init__()
        self.bootstrap_servers = bootstrap_servers
        self.schema_registry = schema_registry
        self.group_id = group_id
        self.topic = topic
        self.schema_subject = schema_subject
        self.avro_producer = AvroProducer({
            'bootstrap.servers': self.bootstrap_servers,
            'schema.registry.url': self.schema_registry,
        },
            default_value_schema=self.value_schema)

    @property
    def value_schema(self):
        if self.schema_subject is None or self.schema_registry is None:
            raise ValueError('Schema subject AND schema_registry required')
        _, schema, _ = SchemaRegistry(self.schema_registry).get_latest_schema(self.schema_subject)
        return schema

    def send_message(self, message):
        if not(message):
            raise ValueError("Message is required")

        # print('self.schema_subject = ', self.schema_subject)
        self.avro_producer.produce(topic=self.topic, value=message)
        self.avro_producer.flush()
        # sleep(0.000001)


class ProbeProducer(object):
    """docstring for Producer"""

    def __init__(self, bootstrap_servers=None):
        super(ProbeProducer, self).__init__()
        self.bootstrap_servers = bootstrap_servers
        self.producer = Producer({
            'bootstrap.servers': self.bootstrap_servers,
        })

    def send_message(self, topic, message, callback):
        if not(message):
            raise ValueError("Message is required")

        self.producer.produce(topic=topic, value=message, callback=callback)
        self.producer.flush()
