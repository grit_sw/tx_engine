#!/usr/bin/env python3
from engine import TransactionEngine

tx_engine = TransactionEngine(
    bootstrap_servers='localhost',
    schema_registry='http://localhost:8081',
    group_id='ariaria_transact_prod',
    topics=['probe-stream'],
)
tx_engine.run()
